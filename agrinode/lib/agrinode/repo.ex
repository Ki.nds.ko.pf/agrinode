defmodule Agrinode.Repo do
  use Ecto.Repo,
    otp_app: :agrinode,
    adapter: Ecto.Adapters.Postgres
end
