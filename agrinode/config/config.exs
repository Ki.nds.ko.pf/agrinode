# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :agrinode,
  ecto_repos: [Agrinode.Repo]

# Configures the endpoint
config :agrinode, AgrinodeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "X5VyzTh4Hiof2P6+UUJKpiopdIfUIV3ajzR+RJ69lNnU7K0h8OcnArNr5FcAS7/r",
  render_errors: [view: AgrinodeWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Agrinode.PubSub,
  live_view: [signing_salt: "eGlg6hIp"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
