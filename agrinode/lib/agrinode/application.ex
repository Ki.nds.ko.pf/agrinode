defmodule Agrinode.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Agrinode.Repo,
      # Start the Telemetry supervisor
      AgrinodeWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Agrinode.PubSub},
      # Start the Endpoint (http/https)
      AgrinodeWeb.Endpoint
      # Start a worker by calling: Agrinode.Worker.start_link(arg)
      # {Agrinode.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Agrinode.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    AgrinodeWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
